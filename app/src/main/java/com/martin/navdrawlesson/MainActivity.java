package com.martin.navdrawlesson;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawer;
    private Toolbar toolbar;
    private TextView textView;
    private NavigationView navigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initToolbar();

        initFab();

        initNavDrawer();

        initMainContentUi();

    }

    private void initMainContentUi() {
        textView = findViewById(R.id.main_textview);
        Switch color_switch = findViewById(R.id.nw_color_switch);
        color_switch.setOnCheckedChangeListener(getColorSwitchListener());
    }

    @NonNull
    private CompoundButton.OnCheckedChangeListener getColorSwitchListener() {
        return new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean state) {
                if (state) {
                    navDrawChangeColor(getResources().getColor(R.color.deepOrange));
                } else navDrawChangeColor(getResources().getColor(R.color.colorPrimary));
            }
        };
    }

    private void navDrawChangeColor(int colorId) {
        navigationView.setItemTextColor(ColorStateList.valueOf(colorId));
        navigationView.setItemIconTintList(ColorStateList.valueOf(colorId));
    }

    private void initNavDrawer() {
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(getNavItemListener());
    }

    @NonNull
    private NavigationView.OnNavigationItemSelectedListener getNavItemListener() {
        return new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();

                switch (id) {
                    case R.id.nav_camera:
                        textView.setText(R.string.textview_nav_camera);
                        break;
                    case R.id.nav_gallery:
                        textView.setText(R.string.textview_nav_gallery);
                        break;
                    case R.id.nav_slideshow:
                        textView.setText(R.string.tv_nav_slideshow);
                        break;
                    case R.id.nav_manage:
                        textView.setText(R.string.tv_nav_manage);
                        break;
                    case R.id.nav_share:
                        textView.setText(R.string.tv_nav_share);
                        break;
                    case R.id.nav_send:
                        textView.setText(R.string.tv_nav_send);
                        break;
                    default:
                        textView.setText(R.string.tv_unknown_source);
                        break;
                }

                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        };
    }

    private void initFab() {
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    private void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
}
